package ru.aushakov.tm.enumerated;

public enum Status {

    PLANNED("Planned"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    private final String displayName;

    private static final Status[] staticValues = values();

    Status(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public static Status toStatus(final String statusId) {
        for (final Status status : staticValues) {
            if (status.name().equalsIgnoreCase(statusId)) return status;
        }
        return null;
    }

}
