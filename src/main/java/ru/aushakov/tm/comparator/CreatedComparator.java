package ru.aushakov.tm.comparator;

import ru.aushakov.tm.api.entity.IHasCreated;

import java.util.Comparator;
import java.util.Date;

public final class CreatedComparator implements Comparator<IHasCreated> {

    private static final CreatedComparator INSTANCE = new CreatedComparator();

    private CreatedComparator() {
    }

    public static CreatedComparator getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(final IHasCreated o1, final IHasCreated o2) {
        if (o1 == null || o2 == null) return ((o1 == null) ? ((o2 == null) ? 0 : -1) : 1);
        final Date created1 = o1.getCreated();
        final Date created2 = o2.getCreated();
        if (created1 == null || created2 == null) return ((created1 == null) ? ((created2 == null) ? 0 : -1) : 1);
        return created1.compareTo(created2);
    }

}
