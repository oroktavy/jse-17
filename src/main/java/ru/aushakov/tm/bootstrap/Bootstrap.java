package ru.aushakov.tm.bootstrap;

import ru.aushakov.tm.api.ServiceLocator;
import ru.aushakov.tm.api.repository.ICommandRepository;
import ru.aushakov.tm.api.repository.IProjectRepository;
import ru.aushakov.tm.api.repository.ITaskRepository;
import ru.aushakov.tm.api.service.ICommandService;
import ru.aushakov.tm.api.service.ILoggerService;
import ru.aushakov.tm.api.service.IProjectService;
import ru.aushakov.tm.api.service.ITaskService;
import ru.aushakov.tm.command.AbstractCommand;
import ru.aushakov.tm.command.project.*;
import ru.aushakov.tm.command.system.*;
import ru.aushakov.tm.command.task.*;
import ru.aushakov.tm.enumerated.Status;
import ru.aushakov.tm.exception.empty.EmptyCommandException;
import ru.aushakov.tm.exception.general.UnknownArgumentException;
import ru.aushakov.tm.exception.general.UnknownCommandException;
import ru.aushakov.tm.repository.CommandRepository;
import ru.aushakov.tm.repository.ProjectRepository;
import ru.aushakov.tm.repository.TaskRepository;
import ru.aushakov.tm.service.CommandService;
import ru.aushakov.tm.service.LoggerService;
import ru.aushakov.tm.service.ProjectService;
import ru.aushakov.tm.service.TaskService;
import ru.aushakov.tm.util.TerminalUtil;

public class Bootstrap implements ServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository, taskRepository);

    private final ILoggerService loggerService = new LoggerService();

    {
        registerCommand(new AboutCommand());
        registerCommand(new VersionCommand());
        registerCommand(new HelpCommand());
        registerCommand(new InfoCommand());
        registerCommand(new ArgumentsCommand());
        registerCommand(new CommandListCommand());

        registerCommand(new TaskListCommand());
        registerCommand(new TaskCreateCommand());
        registerCommand(new TaskClearCommand());
        registerCommand(new TaskViewByIdCommand());
        registerCommand(new TaskViewByIndexCommand());
        registerCommand(new TaskViewByNameCommand());
        registerCommand(new TaskRemoveByIdCommand());
        registerCommand(new TaskRemoveByIndexCommand());
        registerCommand(new TaskRemoveByNameCommand());
        registerCommand(new TaskUpdateByIdCommand());
        registerCommand(new TaskUpdateByIndexCommand());
        registerCommand(new TaskStartByIdCommand());
        registerCommand(new TaskStartByIndexCommand());
        registerCommand(new TaskStartByNameCommand());
        registerCommand(new TaskFinishByIdCommand());
        registerCommand(new TaskFinishByIndexCommand());
        registerCommand(new TaskFinishByNameCommand());
        registerCommand(new TaskChangeStatusByIdCommand());
        registerCommand(new TaskChangeStatusByIndexCommand());
        registerCommand(new TaskChangeStatusByNameCommand());
        registerCommand(new TaskAssignToProjectCommand());
        registerCommand(new TaskUnbindFromProjectCommand());
        registerCommand(new TaskFindAllByProjectIdCommand());

        registerCommand(new ProjectListCommand());
        registerCommand(new ProjectCreateCommand());
        registerCommand(new ProjectClearCommand());
        registerCommand(new ProjectViewByIdCommand());
        registerCommand(new ProjectViewByIndexCommand());
        registerCommand(new ProjectViewByNameCommand());
        registerCommand(new ProjectRemoveByIdCommand());
        registerCommand(new ProjectRemoveByIndexCommand());
        registerCommand(new ProjectRemoveByNameCommand());
        registerCommand(new ProjectUpdateByIdCommand());
        registerCommand(new ProjectUpdateByIndexCommand());
        registerCommand(new ProjectStartByIdCommand());
        registerCommand(new ProjectStartByIndexCommand());
        registerCommand(new ProjectStartByNameCommand());
        registerCommand(new ProjectFinishByIdCommand());
        registerCommand(new ProjectFinishByIndexCommand());
        registerCommand(new ProjectFinishByNameCommand());
        registerCommand(new ProjectChangeStatusByIdCommand());
        registerCommand(new ProjectChangeStatusByIndexCommand());
        registerCommand(new ProjectChangeStatusByNameCommand());
        registerCommand(new ProjectDeepDeleteByIdCommand());

        registerCommand(new ExitCommand());
    }

    private void registerCommand(final AbstractCommand command) {
        if (command == null) throw new EmptyCommandException(command);
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initData() {
        taskService.add("DEMO 2", "222222222222").setStatus(Status.IN_PROGRESS);
        taskService.add("DEMO 3", "333333333333");
        taskService.add("DEMO 1", "111111111111").setStatus(Status.COMPLETED);
        projectService.add("DEMO 2", "222222222222").setStatus(Status.IN_PROGRESS);
        projectService.add("DEMO 3", "333333333333");
        projectService.add("DEMO 1", "111111111111").setStatus(Status.COMPLETED);
    }

    public void run(final String[] args) {
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        initData();
        if (parseArgs(args)) System.exit(0);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = TerminalUtil.nextLine();
            loggerService.command(command);
            try {
                parseCommand(command);
                System.out.println("[OK]");
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    public void parseArg(final String arg) {
        final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null) throw new UnknownArgumentException(arg);
        command.execute();
    }

    public void parseCommand(final String command) {
        final AbstractCommand parsedCommand = commandService.getCommandByName(command);
        if (parsedCommand == null) throw new UnknownCommandException(command);
        parsedCommand.execute();
    }

    public boolean parseArgs(final String[] args) {
        if (args == null || args.length < 1) return false;
        parseArg(args[0]);
        if (args.length > 1) System.out.println("NOTE: Only one argument is supported at a time!");
        return true;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

}
