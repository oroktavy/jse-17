package ru.aushakov.tm.api.service;

import ru.aushakov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    void add(Task task);

    Task add(String name, String description);

    void remove(Task task);

    void clear();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    Task removeOneById(String id);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);

    Task updateOneById(String id, String name, String description);

    Task updateOneByIndex(Integer index, String name, String description);

    Task startOneById(String id);

    Task startOneByIndex(Integer index);

    Task startOneByName(String name);

    Task finishOneById(String id);

    Task finishOneByIndex(Integer index);

    Task finishOneByName(String name);

    Task changeOneStatusById(String id, String statusId);

    Task changeOneStatusByIndex(Integer index, String statusId);

    Task changeOneStatusByName(String name, String statusId);

    Task assignTaskToProject(String taskId, String projectId);

    Task unbindTaskFromProject(String taskId);

    List<Task> findAllTasksByProjectId(String projectId);

}
