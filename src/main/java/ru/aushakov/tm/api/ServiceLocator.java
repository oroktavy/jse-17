package ru.aushakov.tm.api;

import ru.aushakov.tm.api.service.ICommandService;
import ru.aushakov.tm.api.service.IProjectService;
import ru.aushakov.tm.api.service.ITaskService;

public interface ServiceLocator {

    IProjectService getProjectService();

    ITaskService getTaskService();

    ICommandService getCommandService();

}
